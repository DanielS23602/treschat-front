import axios from 'axios';
import Cookies from 'js-cookie';
class AuthService {
    ENDPOINT_PATH = "http://ec2-18-218-62-238.us-east-2.compute.amazonaws.com:8085/api/user/";

    async authenticate(credentials) {
        return axios.post(this.ENDPOINT_PATH + "authenticate", credentials);
    }

    async register(user) {
        return axios.post(this.ENDPOINT_PATH + "register", user);
    }

    setUserLogged(userLogged) {
        Cookies.set("userLogged", userLogged);
    }

    getUserLogged(){
        return Cookies.get("userLogged");
    }

    deleteUserLogged(){
        Cookies.remove("userLogged");
    }
}


export default new AuthService();